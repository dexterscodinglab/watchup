#!/bin/bash
clear
target_website="http://localhost:8000"
urls_to_curl=("http://changeme.example.domain/ntfy-topic" "http://changeme-2.example.domain/ntfy-topic")
content_file="/tmp/website_content.html"
hash_file="/tmp/website_hash.txt"

get_website_hash() {
	curl -s "$target_website" > "$content_file"
	md5sum "$content_file" | awk '{print $1}'
}

curl_urls() {
	for url in "${urls_to_curl[@]}"; do
		curl -s "$url" -d "Website changed" > /dev/null
	done
}

previous_hash=$(cat "$hash_file" 2>/dev/null)

while true; do
	current_time="[`date -u +%Y-%m-%dT%H:%M:%S%Z`]"
	current_hash=$(get_website_hash)

	if [[ "$previous_hash" != "$current_hash" ]]; then
		echo "$current_time \"Website changed, curling URLs...\""
		curl_urls
		echo "$current_hash" > "$hash_file"
		previous_hash="$current_hash" # Update the previous_hash variable within the loop
	else
		echo "$current_time \"Website ok...\""
	fi
	sleep 10
	echo "$current_time \"Checking website...\""
done
