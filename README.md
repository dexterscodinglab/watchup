# Watchup
Watchup is a simple program to monitor a static website for changes, by default it checks once every 60 seconds but this can be modified easily.

## Usage
To use it, edit the script and change the target_website variable, and the ntfy_servers variable. Then, verify the integrity of the file using `sha256sum -c sha256sum.txt` or `sha512sum -c sha512sum.txt`. If it outputs OK, run the script.
